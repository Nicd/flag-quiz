# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

# Configures the endpoint
config :flag_quiz, FlagQuizWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ZeKBbuN9UnxXPr3qvTIaK3qumLoHg9GblL9mkrjJYg3rEhlwgnV4ss5EQ65h5cqs",
  render_errors: [view: FlagQuizWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: FlagQuiz.PubSub,
  live_view: [signing_salt: "lU6F5ijV"],
  code_reloader: Config.config_env() == :dev,
  debug_errors: Config.config_env() == :dev,
  check_origin: Config.config_env() == :prod

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason
