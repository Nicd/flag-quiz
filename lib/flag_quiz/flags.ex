defmodule FlagQuiz.Flags do
  @flags [
    {"AD", ["Andorra"]},
    {"AE",
     [
       "United Arab Emirates",
       "Arab Emirates",
       "Emirates",
       "Arabiemiirikunnat",
       "Emiraatit",
       "Yhdistyneet arabiemiirikunnat"
     ]},
    {"AF", ["Afghanistan", "Afganistan"]},
    {"AG", ["Antigua and Barbuda", "Antigua ja Barbuda"]},
    # {"AI", ["Anguilla"]},
    {"AL", ["Albania"]},
    {"AM", ["Armenia"]},
    {"AO", ["Angola"]},
    # {"AQ", ["Antarctica"]},
    {"AR", ["Argentina", "Argentiina"]},
    # {"AS", ["American Samoa"]},
    {"AT", ["Austria", "Itävalta"]},
    {"AU", ["Australia"]},
    # {"AW", ["Aruba"]},
    # {"AX", ["Åland Islands", "Ahvenanmaa"]},
    {"AZ", ["Azerbaijan", "Azerbaidzan"]},
    {"BA",
     [
       "Bosnia and Herzegovina",
       "Bosnia ja Hertsegovina",
       "Bosnia-Hertsegovina",
       "Bosnia-Herzegovina"
     ]},
    {"BB", ["Barbados"]},
    {"BD", ["Bangladesh"]},
    {"BE", ["Belgium", "Belgia"]},
    {"BF", ["Burkina Faso"]},
    {"BG", ["Bulgaria"]},
    {"BH", ["Bahrain"]},
    {"BI", ["Burundi"]},
    {"BJ", ["Benin"]},
    # {"BL", ["Saint Barthélemy"]},
    # {"BM", ["Bermuda"]},
    {"BN", ["Brunei Darussalam", "Brunei"]},
    {"BO", ["Bolivia, Plurinational State of", "Bolivia"]},
    # {"BQ", ["Caribbean Netherlands"]},
    {"BR", ["Brazil", "Brasilia"]},
    {"BS", ["Bahamas", "The Bahamas", "Bahama"]},
    {"BT", ["Bhutan"]},
    # {"BV", ["Bouvet Island"]},
    {"BW", ["Botswana"]},
    {"BY", ["Belarus", "Valko-Venäjä"]},
    {"BZ", ["Belize"]},
    {"CA", ["Canada", "Kanada"]},
    # {"CC", ["Cocos (Keeling) Islands"]},
    {"CD",
     [
       "Congo, the Democratic Republic of the",
       "Congo-Kinshasa",
       "DRC",
       "DROC",
       "Congo",
       "Kongo",
       "Kongon demokraattinen tasavalta",
       "Kongo-Kinshasa",
       "Kinshasan Kongo"
     ]},
    {"CF", ["Central African Republic", "Central Africa", "Keski-Afrikka"]},
    {"CG", ["Congo", "Congo Rebublic", "Kongo", "Kongon tasavalta"]},
    {"CH", ["Switzerland", "Sveitsi"]},
    {"CI", ["Côte d'Ivoire", "Ivory Coast", "Norsunluurannikko"]},
    # {"CK", ["Cook Islands"]},
    {"CL", ["Chile"]},
    {"CM", ["Cameroon", "Kamerun"]},
    {"CN", ["China", "Kiina"]},
    {"CO", ["Colombia", "Kolumbia"]},
    {"CR", ["Costa Rica"]},
    {"CU", ["Cuba", "Kuuba"]},
    {"CV", ["Cape Verde", "Kap Verde"]},
    # {"CW", ["Curaçao"]},
    # {"CX", ["Christmas Island"]},
    {"CY", ["Cyprus", "Kypros"]},
    {"CZ", ["Czech Republic", "Czechia", "Tsekki", "Tsekki"]},
    {"DE", ["Germany", "Saksa"]},
    {"DJ", ["Djibouti"]},
    {"DK", ["Denmark", "Tanska"]},
    {"DM", ["Dominica"]},
    {"DO", ["Dominican Republic", "Dominikaaninen tasavalta"]},
    {"DZ", ["Algeria"]},
    {"EC", ["Ecuador"]},
    {"EE", ["Estonia", "Viro"]},
    {"EG", ["Egypt", "Egypti"]},
    # {"EH", ["Western Sahara", "Länsi-Sahara"]},
    {"ER", ["Eritrea"]},
    {"ES", ["Spain", "Espanja"]},
    {"ET", ["Ethiopia", "Etiopia"]},
    {"FI", ["Finland", "Suomi"]},
    {"FJ", ["Fiji", "Fidzi"]},
    {"FM", ["Micronesia, Federated States of", "Micronesia", "Mikronesia"]},
    {"FR", ["France", "Ranska"]},
    {"GA", ["Gabon"]},
    {"GB", ["UK", "United Kingdom", "Iso-Britannia", "Yhdistynyt kuningaskunta"]},
    {"GD", ["Grenada"]},
    {"GE", ["Georgia"]},
    # {"GG", ["Guernsey"]},
    {"GH", ["Ghana"]},
    # {"GI", ["Gibraltar"]},
    # {"GL", ["Greenland"]},
    {"GM", ["Gambia"]},
    {"GN", ["Guinea"]},
    # {"GP", ["Guadeloupe"]},
    {"GQ", ["Equatorial Guinea", "Päiväntasaajan Guinea"]},
    {"GR", ["Greece", "Kreikka"]},
    # {"GS", ["South Georgia and the South Sandwich Islands"]},
    {"GT", ["Guatemala"]},
    # {"GU", ["Guam"]},
    {"GW", ["Guinea-Bissau"]},
    {"GY", ["Guyana"]},
    # {"HK", ["Hong Kong"]},
    # {"HM", ["Heard Island and McDonald Islands"]},
    {"HN", ["Honduras"]},
    {"HR", ["Croatia", "Kroatia"]},
    {"HT", ["Haiti"]},
    {"HU", ["Hungary", "Unkari"]},
    {"ID", ["Indonesia"]},
    {"IE", ["Ireland", "Irlanti"]},
    {"IL", ["Israel"]},
    # {"IM", ["Isle of Man"]},
    {"IN", ["India", "Intia"]},
    # {"IO", ["British Indian Ocean Territory"]},
    {"IQ", ["Iraq", "Irak"]},
    {"IR", ["Iran, Islamic Republic of", "Iran"]},
    {"IS", ["Iceland", "Islanti"]},
    {"IT", ["Italy", "Italia"]},
    # {"JE", ["Jersey"]},
    {"JM", ["Jamaica", "Jamaika"]},
    {"JO", ["Jordan", "Jordania"]},
    {"JP", ["Japan", "Japani"]},
    {"KE", ["Kenya", "Kenia"]},
    {"KG", ["Kyrgyzstan", "Kirgisia"]},
    {"KH", ["Cambodia", "Kambodza"]},
    {"KI", ["Kiribati"]},
    {"KM", ["Comoros", "Komorit", "Komorien liitto"]},
    {"KN", ["Saint Kitts and Nevis", "Saint Kitts ja Nevis"]},
    {"KP",
     [
       "Korea, Democratic People's Republic of",
       "Democratic People's Republic of Korea",
       "North Korea",
       "Pohjois-Korea"
     ]},
    {"KR", ["Korea, Republic of", "Korea", "Republic of Korea", "South Korea", "Etelä-Korea"]},
    {"KW", ["Kuwait"]},
    # {"KY", ["Cayman Islands"]},
    {"KZ", ["Kazakhstan", "Kazakstan"]},
    {"LA", ["Lao People's Democratic Republic", "Laos"]},
    {"LB", ["Lebanon", "Libanon"]},
    {"LC", ["Saint Lucia"]},
    {"LI", ["Liechtenstein"]},
    {"LK", ["Sri Lanka"]},
    {"LR", ["Liberia"]},
    {"LS", ["Lesotho"]},
    {"LT", ["Lithuania", "Liettua"]},
    {"LU", ["Luxembourg", "Luxemburg"]},
    {"LV", ["Latvia"]},
    {"LY", ["Libya"]},
    {"MA", ["Morocco", "Marokko"]},
    {"MC", ["Monaco"]},
    {"MD", ["Moldova, Republic of", "Moldova"]},
    {"ME", ["Montenegro"]},
    # {"MF", ["Saint Martin"]},
    {"MG", ["Madagascar", "Madagaskar"]},
    {"MH", ["Marshall Islands", "Marshallsaaret", "Marshallinsaaret"]},
    {"MK",
     ["Macedonia, the former Yugoslav Republic of", "Macedonia", "Pohjois-Makedonia", "Makedonia"]},
    {"ML", ["Mali"]},
    {"MM", ["Myanmar", "Burma"]},
    {"MN", ["Mongolia"]},
    # {"MO", ["Macao"]},
    # {"MP", ["Northern Mariana Islands"]},
    # {"MQ", ["Martinique"]},
    {"MR", ["Mauritania"]},
    # {"MS", ["Montserrat"]},
    {"MT", ["Malta"]},
    {"MU", ["Mauritius"]},
    {"MV", ["Maldives", "Malediivit"]},
    {"MW", ["Malawi"]},
    {"MX", ["Mexico", "Meksiko"]},
    {"MY", ["Malaysia", "Malesia"]},
    {"MZ", ["Mozambique", "Mosambik"]},
    {"NA", ["Namibia"]},
    # {"NC", ["New Caledonia"]},
    {"NE", ["Niger"]},
    # {"NF", ["Norfolk Island"]},
    {"NG", ["Nigeria"]},
    {"NI", ["Nicaragua"]},
    {"NL", ["Netherlands", "Alankomaat", "Hollanti"]},
    {"NO", ["Norway", "Norja"]},
    {"NP", ["Nepal"]},
    {"NR", ["Nauru"]},
    # {"NU", ["Niue"]},
    {"NZ", ["New Zealand", "Uusi-Seelanti"]},
    {"OM", ["Oman"]},
    {"PA", ["Panama"]},
    {"PE", ["Peru"]},
    # {"PF", ["French Polynesia"]},
    {"PG", ["Papua New Guinea", "Papua-Uusi-Guinea"]},
    {"PH", ["Philippines", "Filippiinit"]},
    {"PK", ["Pakistan"]},
    {"PL", ["Poland", "Puola"]},
    # {"PM", ["Saint Pierre and Miquelon"]},
    # {"PN", ["Pitcairn"]},
    # {"PR", ["Puerto Rico"]},
    # {"PS", ["Palestine", "Palestiina", "Palestiinalaisalueet"]},
    {"PT", ["Portugal", "Portugali"]},
    {"PW", ["Palau"]},
    {"PY", ["Paraguay"]},
    {"QA", ["Qatar"]},
    # {"RE", ["Réunion"]},
    {"RO", ["Romania"]},
    {"RS", ["Serbia"]},
    {"RU", ["Russian Federation", "Russia", "Venäjä"]},
    {"RW", ["Rwanda", "Ruanda"]},
    {"SA", ["Saudi Arabia"]},
    {"SB", ["Solomon Islands", "Salomonsaaret"]},
    {"SC", ["Seychelles", "Seychellit"]},
    {"SD", ["Sudan"]},
    {"SE", ["Sweden", "Ruotsi"]},
    {"SG", ["Singapore"]},
    # {"SH", ["Saint Helena, Ascension and Tristan da Cunha"]},
    {"SI", ["Slovenia"]},
    # {"SJ", ["Svalbard and Jan Mayen Islands"]},
    {"SK", ["Slovakia"]},
    {"SL", ["Sierra Leone"]},
    {"SM", ["San Marino"]},
    {"SN", ["Senegal"]},
    {"SO", ["Somalia"]},
    {"SR", ["Suriname", "Surinam"]},
    {"SS", ["South Sudan", "Etelä-Sudan"]},
    {"ST", ["Sao Tome and Principe", "Sao Tome ja Principe"]},
    {"SV", ["El Salvador"]},
    # {"SX", ["Sint Maarten (Dutch part)"]},
    {"SY", ["Syrian Arab Republic", "Syria", "Syyria"]},
    {"SZ", ["Swaziland", "Swazimaa"]},
    # {"TC", ["Turks and Caicos Islands"]},
    {"TD", ["Chad", "Tsad"]},
    # {"TF", ["French Southern Territories"]},
    {"TG", ["Togo"]},
    {"TH", ["Thailand", "Thaimaa"]},
    {"TJ", ["Tajikistan", "Tadzikistan"]},
    # {"TK", ["Tokelau"]},
    {"TL", ["Timor-Leste", "East-Timor", "Itä-Timor"]},
    {"TM", ["Turkmenistan"]},
    {"TN", ["Tunisia"]},
    {"TO", ["Tonga"]},
    {"TR", ["Turkey", "Turkki"]},
    {"TT", ["Trinidad and Tobago", "Trinidad ja Tobaco"]},
    {"TV", ["Tuvalu"]},
    {"TW", ["Taiwan", "Republic of China"]},
    {"TZ", ["Tanzania, United Republic of", "Tanzania", "Tansania"]},
    {"UA", ["Ukraine", "Ukraina"]},
    {"UG", ["Uganda"]},
    # {"UM", ["US Minor Outlying Islands"]},
    # {"UN", ["United Nations"]},
    {"US",
     [
       "United States",
       "USA",
       "Yhdysvallat",
       "United States of America",
       "The United States of America"
     ]},
    {"UY", ["Uruguay"]},
    {"UZ", ["Uzbekistan"]},
    {"VA", ["Holy See", "Vatican City", "Vatican", "Vatikaani", "Vatikaanivaltio"]},
    {"VC", ["Saint Vincent and the Grenadines", "Saint Vincent ja Grenadiinit"]},
    {"VE", ["Venezuela, Bolivarian Republic of", "Venezuela"]},
    # {"VG", ["Virgin Islands, British"]},
    # {"VI", ["Virgin Islands, U.S."]},
    {"VN", ["Viet Nam", "Vietnam"]},
    {"VU", ["Vanuatu"]},
    # {"WF", ["Wallis and Futuna Islands"]},
    {"WS", ["Samoa"]},
    # {"XK", ["Kosovo"]},
    {"YE", ["Yemen", "Jemen"]},
    # {"YT", ["Mayotte"]},
    {"ZA", ["South Africa", "Etelä-Afrikka"]},
    {"ZM", ["Zambia", "Sambia"]},
    {"ZW", ["Zimbabwe"]}
  ]

  def get_quiz_set(amount) do
    Enum.take_random(@flags, amount)
  end

  def get_all() do
    @flags
  end

  def get_filename(code) do
    lower = String.downcase(code)
    :crypto.hash(:md5, lower) |> Base.encode16(case: :lower)
  end
end
