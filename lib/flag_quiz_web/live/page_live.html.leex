<%= cond do %>
  <% not @socket.connected? -> %>
    <h1>Loading...</h1>
    <p>You will need JavaScript turned on to use this website.</p>
  <% not @in_game? -> %>
    <h1>Lippuvisa / Flag Quiz</h1>
    <p>
      Lippuvisassa lippukone arpoo sinulle <%= Application.get_env(:flag_quiz, :q_amount) %> lippua, joista jokaisen kohdalla sinulla on <%= Application.get_env(:flag_quiz, :q_time) %> sekuntia aikaa kirjoittaa vastauskenttään kyseessä olevan valtion nimi. Oikeaa vastausta saat arvata niin monta kertaa kuin ehdit aikarajan puitteissa. Lippukone hyväksyy vastauksen sekä englannin että suomen kielellä. Kirjoita valtion nimi siinä muodossa, jota valtiosta yleisesti käytetään (esim. Suomi, ei Suomen tasavalta, tai Pohjois-Korea, ei Korean demokraattinen kansantasavalta). Isoilla ja pienillä alkukirjaimilla ei kuitenkaan ole väliä, kuten ei myöskään diakriittisillä merkeillä (esim. ã, í, ž jne), väliviivoilla, välilyönneillä ja ääkkösillä. Muita poikkeamia kirjoitusasussa lippukone ei salli. Paina enter-näppäintä kirjoitettuasi vastauksen. Mikäli aika loppuu, visa päättyy.
    </p>

    <p>Onnea matkaan!</p>

    <p>
      In Flag Quiz the game will randomly select <%= Application.get_env(:flag_quiz, :q_amount) %> flags and you have <%= Application.get_env(:flag_quiz, :q_time) %> seconds of time to guess each one's country. You can attempt as many answers as you have time to. You can answer in English or Finnish. Prefer the colloquial name of a country to the official one, for example "North Korea" instead of "Democratic People's Republic of Korea". Characters outside a-z will be converted to a-z and other special characters and spaces are ignored. Press enter to submit you answer. If the time ends, the game ends.
    </p>

    <p>Good luck!</p>

    <form phx-submit="start-game">
      <button type="submit">Aloita peli / Start game</button>
    </form>
  <% @victory? -> %>
    <h1>Voitit pelin! / You won!</h1>
    <p>Palkintosi / Your reward:</p>
    <p id="reward"><%= Application.get_env(:flag_quiz, :secret_msg) %></p>
    <form phx-submit="start-game">
      <button type="submit">Pelaa uudestaan / Play again</button>
    </form>
  <% true -> %>
    <h1 id="title">Lippuvisa / Flag Quiz</h1>
    <div id="counter"><%= @total - Enum.count(@quiz_set) %> / <%= @total %></div>
    <div
      id="flag"
      class="flag-icon-background <%= if @valid?, do: "flag-valid" %> <%= if @failed?, do: "flag-failed" %>"
      style="background-image: url('<%= Routes.static_path(@socket, "/flags/4x3/#{@flag_code}.svg") %>');"
    ></div>
    <p id="timer">
      <%= if not @valid?, do: @time %>
    </p>
    <form id="input-form" method="post" phx-submit="flag-input">
      <input
        id="country-input"
        name="country-input"
        type="text"
        placeholder="Mikä maa? / Which country?"
        <%= if @valid? or @time == 0, do: "disabled" %>
        phx-hook="CountryInput"
      />
    </form>
<% end %>
