defmodule FlagQuizWeb.PageLive do
  use FlagQuizWeb, :live_view

  require Logger

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     assign(socket,
       in_game?: false,
       tick_timer: nil,
       fail_timer: nil,
       total: Application.get_env(:flag_quiz, :q_amount)
     )}
  end

  @impl true
  def handle_event(event, params, socket)

  def handle_event("start-game", _, socket) do
    quiz_set = FlagQuiz.Flags.get_quiz_set(Application.get_env(:flag_quiz, :q_amount))
    {:noreply, assign(socket, in_game?: true) |> init_flag(quiz_set)}
  end

  def handle_event("flag-input", params, socket) do
    if not socket.assigns.valid? do
      name = Map.get(params, "country-input", "") |> process_name()
      valid = name in socket.assigns.flag_names

      if not is_nil(socket.assigns.fail_timer),
        do: Process.cancel_timer(socket.assigns.fail_timer)

      socket =
        if valid do
          if not is_nil(socket.assigns.tick_timer),
            do: Process.cancel_timer(socket.assigns.tick_timer)

          Process.send_after(self(), :next, 2_000)
          socket
        else
          socket
          |> push_event("clear-input", %{})
          |> assign(fail_timer: Process.send_after(self(), :clear_failed, 1_000))
        end

      {:noreply, assign(socket, valid?: valid, failed?: not valid)}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info(msg, socket)

  def handle_info(:next, %{assigns: %{quiz_set: []}} = socket) do
    {:noreply, assign(socket, victory?: true)}
  end

  def handle_info(:next, socket) do
    {:noreply, init_flag(socket, socket.assigns.quiz_set)}
  end

  def handle_info(:tick, %{assigns: %{time: time}} = socket) when time <= 1 do
    Process.send_after(self(), :reset, 2_000)

    if not is_nil(socket.assigns.fail_timer),
      do: Process.cancel_timer(socket.assigns.fail_timer)

    {:noreply, assign(socket, time: 0, failed?: true)}
  end

  def handle_info(:tick, socket) do
    tick_timer = Process.send_after(self(), :tick, 1_000)
    {:noreply, assign(socket, time: socket.assigns.time - 1, tick_timer: tick_timer)}
  end

  def handle_info(:reset, socket) do
    {:noreply, assign(socket, in_game?: false)}
  end

  def handle_info(:clear_failed, %{assigns: %{time: time}} = socket) when time > 0 do
    {:noreply, assign(socket, failed?: false)}
  end

  def handle_info(msg, socket) do
    Logger.debug("Got unknown msg: #{inspect(msg)}")
    {:noreply, socket}
  end

  defp init_flag(socket, [{flag_code, flag_names} | rest]) do
    Process.send_after(self(), :tick, 1_000)

    assign(socket,
      flag_code: FlagQuiz.Flags.get_filename(flag_code),
      flag_names: Enum.map(flag_names, &process_name/1),
      time: Application.get_env(:flag_quiz, :q_time),
      valid?: false,
      failed?: false,
      victory?: false,
      quiz_set: rest
    )
    |> push_event("clear-input", %{})
  end

  defp process_name(name) do
    name
    |> String.downcase()
    |> String.normalize(:nfc)
    |> String.replace("the ", "")
    |> String.replace(" and ", " & ")
    |> String.replace(" ja ", " & ")
    |> String.replace("saint ", "st ")
    |> String.replace(["ä", "ã", "é", "í", "ö", "ô", "š", "ž", "ç"], &replace_special/1)
    |> String.replace(~R/\s*/, "")
    |> String.replace("-", "")
    |> String.replace("'", "")
    |> String.replace(",", "")
  end

  defp replace_special(char)

  defp replace_special("ä"), do: "a"
  defp replace_special("ö"), do: "o"
  defp replace_special("ô"), do: "o"
  defp replace_special("š"), do: "s"
  defp replace_special("ž"), do: "z"
  defp replace_special("ç"), do: "c"
  defp replace_special("ã"), do: "a"
  defp replace_special("é"), do: "e"
  defp replace_special("í"), do: "i"
end
